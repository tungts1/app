package com.tungts.food.activity.activity.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.tungts.food.R;
import com.tungts.food.activity.activity.CustomerActionBar;
import com.tungts.food.activity.activity.TabRecipes;
import com.tungts.food.activity.activity.fragment.FragmentTab;
import com.tungts.food.activity.activity.interfaces.ItemClickMenuNavi;
import com.tungts.food.activity.activity.interfaces.NaviListener;
import com.tungts.food.activity.activity.interfaces.OnRefreshFragment;
import com.tungts.food.activity.activity.model.Categories;
import com.tungts.food.activity.activity.model.Recipes;

import java.util.ArrayList;

import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_CATEGORIES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_COMPOSE;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_HOME;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_PROFILE;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_RECIPES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_SETTINGS;

public class HomeActivity extends AppCompatActivity implements NaviListener,DrawerLayout.DrawerListener,ItemClickMenuNavi{

    DrawerLayout drawerLayout;
    CustomerActionBar actionBar;
    ImageView imgMenuNavi;


    OnRefreshFragment onRefreshSelected;
    FragmentTab fragmentTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        setContentView(R.layout.activity_home);
        addControls();
        addEvents();
    }

    private void addControls() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        actionBar = (CustomerActionBar) findViewById(R.id.actionBar);
        actionBar.setNaviListener(this);
        fragmentTab = new FragmentTab(false);
        addFragment(fragmentTab);
        imgMenuNavi = (ImageView) actionBar.findViewById(R.id.imgMenuNavi);
    }


    private void addEvents() {
        drawerLayout.setDrawerListener(this);
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void closeDrawer(){
        if(drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void addFragment(Fragment fragment){
        if (fragment != null){
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.contentFrame, fragment);
            ft.commit();
        }
    }

    private void replaceFragment(Fragment fragment){
        if (fragment != null){
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.contentFrame, fragment);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMenuNavi() {
        if(!drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.openDrawer(GravityCompat.START);
        } else {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void OnSearch() {
        showToast("Search");
        Intent intent = new Intent(HomeActivity.this,SearchActivity.class);
        startActivityForResult(intent,999);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999 && resultCode == 1000){
            String query = data.getStringExtra("QUERY");
            if (getOnRefreshSelected() != null){
                onRefreshSelected.refreshData(query);
            }
        }
    }

    @Override
    public void onItemClickMenuNavi(int position) {
        switch (position){
            case MENU_HOME:
                closeDrawer();
                break;
            case MENU_COMPOSE:
                closeDrawer();
                break;
            case MENU_RECIPES:
                if(getOnRefreshSelected() !=null){
                    onRefreshSelected.refreshFragment(true);
                }
                closeDrawer();
                break;
            case MENU_CATEGORIES:
                if(getOnRefreshSelected() !=null){
                    onRefreshSelected.refreshFragment(false);
                }
                closeDrawer();
                break;
            case MENU_PROFILE:
                closeDrawer();
                break;
            case MENU_SETTINGS:
                closeDrawer();
                break;
        }
    }

    public OnRefreshFragment getOnRefreshSelected() {
        return onRefreshSelected;
    }

    public void setOnRefreshSelected(OnRefreshFragment onRefreshSelected) {
        this.onRefreshSelected = onRefreshSelected;
    }

    private static final float END_SCALE = 0.7f;

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        //Scale
        float diffScaleOffset = slideOffset * (1 - END_SCALE);
        float offsetScale = 1 - diffScaleOffset;
        fragmentTab.getView().setScaleX(offsetScale);
        fragmentTab.getView().setScaleY(offsetScale);

        //Translate
        float xOffset = drawerView.getWidth() * slideOffset;
        float xOffsetDiff = fragmentTab.getView().getWidth() * diffScaleOffset/2;
        float xTranslation = xOffset - xOffsetDiff;
        fragmentTab.getView().setTranslationX(xTranslation);

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        actionBar.setTitle("Menu");
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        actionBar.setTitle("Recipes");
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
