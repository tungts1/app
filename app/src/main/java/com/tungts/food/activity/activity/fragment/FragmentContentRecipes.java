package com.tungts.food.activity.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tungts.food.R;
import com.tungts.food.activity.activity.activity.RecipesActivity;
import com.tungts.food.activity.activity.adapter.ImageAdapter;
import com.tungts.food.activity.activity.adapter.RecipesAdapter;
import com.tungts.food.activity.activity.interfaces.RecycleViewItemClick;
import com.tungts.food.activity.activity.model.Categories;
import com.tungts.food.activity.activity.model.Recipes;

import java.util.ArrayList;

/**
 * Created by tungts on 7/13/2017.
 */

public class FragmentContentRecipes extends Fragment {
    boolean flag;
    View root;
    RelativeLayout rltContentRecipes;
    RecyclerView rcvRecContent;
    ArrayList arrRec;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_content_recipes,container,false);
        RecipesActivity recipesActivity = (RecipesActivity) getActivity();
        flag = recipesActivity.getFlag();
        arrRec = new ArrayList();
        innitData();
        innitContentRecipes();
        return root;
    }

    private void innitData() {
        for (int i = 0; i < 50; i++) {
            if (i % 6 == 0) {
                arrRec.add(new Recipes("Sweet Sorbet", "Selene Setty"));
            } else if (i % 6 == 1) {
                arrRec.add(new Recipes("Mandarian Sorbet", "John Doe"));
            } else if (i % 6 == 2) {
                arrRec.add(new Recipes("Blackberry Sorbet", "Stansil Kirilov"));
            } else if (i % 6 == 3) {
                arrRec.add(new Recipes("Mango Sorbet", "Steve Thomas"));
            } else if (i % 6 == 4) {
                arrRec.add(new Recipes("Strawberry Sorbet", "Mark Kratzman"));
            } else if (i % 6 == 5) {
                arrRec.add(new Recipes("Apple Sorbet", "Mical"));
            }
        }
    }

    private void innitContentRecipes() {
        //content Recipes
        rltContentRecipes = (RelativeLayout) root.findViewById(R.id.contentRecipes);
        RecyclerView rcvImage = (RecyclerView) root.findViewById(R.id.rcvImage);
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0;i<11;i++){
            arr.add(i);
        }
        ImageAdapter imageAdapter = new ImageAdapter(getContext(),arr);
        rcvImage.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        rcvImage.setAdapter(imageAdapter);

        //when click categories
        rcvRecContent = (RecyclerView) root.findViewById(R.id.rcvRecContent);
        ArrayList<Recipes> arrRecipes = arrRec;
        RecipesAdapter recipesAdapter = new RecipesAdapter(getContext(),arrRec,RecipesAdapter.RECIPES);
        recipesAdapter.setRecycleViewItemClick(new RecycleViewItemClick() {
            @Override
            public void itemClick(int position, int type) {
                Intent intent = new Intent(getContext(),RecipesActivity.class);
                startActivity(intent);
            }
        });
        rcvRecContent.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvRecContent.setAdapter(recipesAdapter);

        if (flag){
            rltContentRecipes.setVisibility(View.INVISIBLE);
            rcvRecContent.setVisibility(View.VISIBLE);
        } else {
            rltContentRecipes.setVisibility(View.VISIBLE);
            rcvRecContent.setVisibility(View.INVISIBLE);
        }
    }

}
