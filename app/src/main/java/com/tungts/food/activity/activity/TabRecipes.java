package com.tungts.food.activity.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tungts.food.R;
import com.tungts.food.activity.activity.activity.HomeActivity;
import com.tungts.food.activity.activity.activity.RecipesActivity;
import com.tungts.food.activity.activity.adapter.ImageAdapter;
import com.tungts.food.activity.activity.adapter.RecipesAdapter;
import com.tungts.food.activity.activity.interfaces.ItemClickMenuNavi;
import com.tungts.food.activity.activity.interfaces.OnRefreshFragment;
import com.tungts.food.activity.activity.interfaces.RecycleViewItemClick;
import com.tungts.food.activity.activity.model.Categories;
import com.tungts.food.activity.activity.model.Recipes;

import java.util.ArrayList;

import static com.tungts.food.activity.activity.adapter.RecipesAdapter.CATEGORIES;
import static com.tungts.food.activity.activity.adapter.RecipesAdapter.RECIPES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_CATEGORIES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_RECIPES;

/**
 * Created by tungts on 7/12/2017.
 */

public class TabRecipes extends Fragment implements OnRefreshFragment{

    View root;
    RecyclerView rcvRec,rcvCat;
    RecipesAdapter recipesAdapter;
    ArrayList<Recipes> arrRecipes; ArrayList<Recipes> coppyRecipes = new ArrayList<>();
    ArrayList<Categories> arrCategories;ArrayList<Categories> coppyCategories = new ArrayList<>();
    Context context;
    boolean isRecipes = true;

    public TabRecipes(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.tab_recipes, container, false);
        Log.e("ST","1");
        addControls();
        addEvents();
        return root;
    }

    private void addEvents() {

    }

    private void addControls() {
        rcvRec = (RecyclerView) root.findViewById(R.id.rcvTabRecipes);
        rcvCat = (RecyclerView) root.findViewById(R.id.rcvCat);
        arrRecipes = new ArrayList<>();
        arrCategories = new ArrayList<>();
        innitData();coppyRecipes.addAll(arrRecipes);coppyCategories.addAll(arrCategories);
        innitRcvRec();
        innitRcvCat();
        try {
            ((HomeActivity)getActivity()).setOnRefreshSelected(this);
        } catch (Exception e){
            e.printStackTrace();
        }
//        innitContentRecipes();
    }



    public void innitRcvCat() {
        recipesAdapter = new RecipesAdapter(getContext(), arrCategories, CATEGORIES);
        recipesAdapter.setRecycleViewItemClick(new RecycleViewItemClick() {
            @Override
            public void itemClick(int position, int type) {
//                refreshFragment(true);
                Intent intent = new Intent(getContext(), RecipesActivity.class);
                intent.putExtra("FLAG",true);
                startActivity(intent);
            }
        });
        rcvCat.setAdapter(recipesAdapter);
        rcvCat.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    public void innitRcvRec() {
        recipesAdapter = new RecipesAdapter(getContext(), arrRecipes, RECIPES);
        recipesAdapter.setRecycleViewItemClick(new RecycleViewItemClick() {
            @Override
            public void itemClick(int position,int type) {
                Toast.makeText(getContext(), arrRecipes.get(position).getNameFood(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), RecipesActivity.class);
                startActivity(intent);
            }
        });
        rcvRec.setAdapter(recipesAdapter);
        rcvRec.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void innitData() {
        for (int i = 0; i < 50; i++) {
            if (i % 6 == 0) {
                arrRecipes.add(new Recipes("Sweet Sorbet", "Selene Setty"));
                arrCategories.add(new Categories("Appetizer",69));
            } else if (i % 6 == 1) {
                arrRecipes.add(new Recipes("Mandarian Sorbet", "John Doe"));
                arrCategories.add(new Categories("Bevarages",42));
            } else if (i % 6 == 2) {
                arrRecipes.add(new Recipes("Blackberry Sorbet", "Stansil Kirilov"));
                arrCategories.add(new Categories("Breakfasts",87));
            } else if (i % 6 == 3) {
                arrRecipes.add(new Recipes("Mango Sorbet", "Steve Thomas"));
                arrCategories.add(new Categories("Desserts",34));
            } else if (i % 6 == 4) {
                arrRecipes.add(new Recipes("Strawberry Sorbet", "Mark Kratzman"));
                arrCategories.add(new Categories("Main Dishes",50));
            } else if (i % 6 == 5) {
                arrRecipes.add(new Recipes("Apple Sorbet", "Mical"));
                arrCategories.add(new Categories("Side Dishs",66));
            }
        }
    }

    private void filter(String s) {
        if (isRecipes){
            ArrayList<Recipes> arr = new ArrayList<>();
            if (s.isEmpty()){
                arr.addAll(coppyRecipes);
            } else {
                for (Recipes item : coppyRecipes){
                    if (item.getNameFood().toLowerCase().contains(s.toLowerCase())) {arr.add(item);Log.e("Search",item.getNameFood());}
                }
            }
            arrRecipes.clear();
            arrRecipes.addAll(arr);
            recipesAdapter.notifyDataSetChanged();
        } else {
            ArrayList<Categories> arr = new ArrayList<>();
            if (s.isEmpty()){
                arr.addAll(coppyCategories);
            } else {
                for (Categories item : coppyCategories){
                    if (item.getNameCat().toLowerCase().contains(s.toLowerCase())) arr.add(item);
                }
            }
            arrCategories.clear();arrCategories.addAll(arr);
            recipesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void refreshFragment(boolean flag) {
        if (flag == true){
            arrRecipes.clear();arrRecipes.addAll(coppyRecipes);recipesAdapter.notifyDataSetChanged();
            rcvRec.setVisibility(View.VISIBLE);
            rcvCat.setVisibility(View.INVISIBLE);
        } else if (flag == false){
            arrCategories.clear();arrCategories.addAll(coppyCategories);recipesAdapter.notifyDataSetChanged();
            rcvRec.setVisibility(View.INVISIBLE);
            rcvCat.setVisibility(View.VISIBLE);
        }
        isRecipes = flag;
    }

    @Override
    public void refreshData(String s) {
        Toast.makeText(getContext(), "s"+s, Toast.LENGTH_SHORT).show();
        filter(s);
    }

}

