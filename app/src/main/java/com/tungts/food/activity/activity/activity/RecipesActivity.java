package com.tungts.food.activity.activity.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.tungts.food.R;
import com.tungts.food.activity.activity.CustomerActionBar;
import com.tungts.food.activity.activity.fragment.FragmentTab;
import com.tungts.food.activity.activity.interfaces.NaviListener;

import java.util.ArrayList;

public class RecipesActivity extends AppCompatActivity implements NaviListener{

    CustomerActionBar actionBar;
    boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_);
        actionBar = (CustomerActionBar) findViewById(R.id.recipesActionBar);
        actionBar.setTitle("Recipes");
        actionBar.setNaviListener(this);
        ImageView img = (ImageView) actionBar.findViewById(R.id.imgMenuNavi);
        img.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        ImageView imgNote = (ImageView) actionBar.findViewById(R.id.imgSearch);
        imgNote.setImageDrawable(getResources().getDrawable(R.drawable.ic_compose));
        addFragment(new FragmentTab(true));
        Intent intent = getIntent();
        flag = intent.getBooleanExtra("FLAG",false);
    }

    public boolean getFlag(){
        return flag;
    }

    private void addFragment(Fragment fragment){
        if (fragment != null){
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.contentRecipes, fragment);
            ft.commit();
        }
    }


    @Override
    public void onMenuNavi() {
        onBackPressed();
    }

    @Override
    public void OnSearch() {

    }
}
