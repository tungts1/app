package com.tungts.food.activity.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.tungts.food.R;
import com.tungts.food.activity.activity.TabProfile;
import com.tungts.food.activity.activity.TabRecipes;
import com.tungts.food.activity.activity.TabWine;
import com.tungts.food.activity.activity.adapter.ViewPagerAdapter;

/**
 * Created by tungts on 7/12/2017.
 */

public class FragmentTab extends Fragment implements TabHost.OnTabChangeListener{

    private FragmentTabHost fragmentTabHost;
    private TabRecipes tabRecipes;
    private View root;
    private boolean isContent;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    public FragmentTab(boolean isContent){
        this.isContent = isContent;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_tab,container, false);
        fragmentTabHost = getFragmentTabHost();
        fragmentTabHost.getTabWidget().setDividerDrawable(R.drawable.devider);
        fragmentTabHost.getTabWidget().setBackgroundResource(R.drawable.reg_bg);
        fragmentTabHost.getTabWidget().getLayoutParams().height = (int) convertDpToPixel(40,getContext());
        for(int i = 0; i < fragmentTabHost.getTabWidget().getChildCount(); i++) {
            View v = fragmentTabHost.getTabWidget().getChildAt(i);
            TextView tv = (TextView)v.findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#dcd1d1"));
            if(tv == null) {
                continue;
            }
            if (i == 0) v.setBackgroundResource(R.drawable.tab_bg_sel);
            else v.setBackgroundResource(R.drawable.reg_bg);
        }
        fragmentTabHost.setOnTabChangedListener(this);
//        viewPager = (ViewPager) root.findViewById(R.id.viewPager);
//        setupViewPager(viewPager);
//        tabLayout = (TabLayout) root.findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);
//
//        TextView view = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.customer_tab,null);
//        view.setText("aaa");
//        tabLayout.getTabAt(0).setCustomView(view);

        return fragmentTabHost;
    }

//    private void setupViewPager(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
//        adapter.addFragment(new TabRecipes(), "Recipes");
//        adapter.addFragment(new TabWine(), "Wine");
//        adapter.addFragment(new TabProfile(), "Profile");
//        viewPager.setAdapter(adapter);
//    }

    public TabRecipes getTabRecipes(){
        tabRecipes = (TabRecipes) getChildFragmentManager().findFragmentById(R.id.tabRecipes);
        return tabRecipes;
    }

    private FragmentTabHost getFragmentTabHost() {
        FragmentTabHost fragmentTabHost = new android.support.v4.app.FragmentTabHost(getActivity());
        fragmentTabHost.setup(getActivity(),getChildFragmentManager(),R.id.tabhost);

        TabHost.TabSpec recipesSpec = fragmentTabHost.newTabSpec("Recipes");
        recipesSpec.setIndicator("Recipes");
        if (!isContent){
            fragmentTabHost.addTab(recipesSpec,TabRecipes.class,null);
        } else {
            fragmentTabHost.addTab(recipesSpec,FragmentContentRecipes.class,null);
        }

        TabHost.TabSpec wineSpec = fragmentTabHost.newTabSpec("Wine");
        wineSpec.setIndicator("Wine");
        fragmentTabHost.addTab(wineSpec,TabWine.class,null);

        TabHost.TabSpec profileSpec = fragmentTabHost.newTabSpec("Profile");
        profileSpec.setIndicator("Profile");
        fragmentTabHost.addTab(profileSpec,TabProfile.class,null);

        return fragmentTabHost;
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private void setSeletedBackground(int position){
        View v = fragmentTabHost.getTabWidget().getChildAt(position);
        TextView tv = (TextView)v.findViewById(android.R.id.title);
        tv.setTextColor(Color.WHITE);
        v.setBackgroundResource(R.drawable.tab_bg_sel);
        for (int i = 0;i<3;i++){
            if (i != position){
                View v1 = fragmentTabHost.getTabWidget().getChildAt(i);
                TextView tv1 = (TextView)v1.findViewById(android.R.id.title);
                tv1.setTextColor(Color.parseColor("#dcd1d1"));
                v1.setBackgroundResource(R.drawable.reg_bg);
            }
        }
    }

    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "Recipes":
                setSeletedBackground(0);
                break;
            case "Wine":
                setSeletedBackground(1);
                break;
            case "Profile":
                setSeletedBackground(2);
                break;
            default:break;
        }
    }

}
