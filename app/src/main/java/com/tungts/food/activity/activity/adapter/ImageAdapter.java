package com.tungts.food.activity.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tungts.food.R;

import java.util.List;

/**
 * Created by tungts on 7/13/2017.
 */

public class ImageAdapter  extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder>{

    private Context context;
    private List list;
    private LayoutInflater layoutInflater;

    public ImageAdapter(Context context,List list){
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.item_image,null);
        return new ImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        int i = (int) list.get(position);
        if (i%2 == 0){
            Picasso.with(context).load(R.drawable.recipe1).into(holder.img);
        } else {
            Picasso.with(context).load(R.drawable.recipe2).into(holder.img);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder{

        private ImageView img;
        public ImageViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
        }

    }
}
