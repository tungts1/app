package com.tungts.food.activity.activity.interfaces;

/**
 * Created by tungts on 7/13/2017.
 */

public interface OnRefreshFragment {
    public void refreshFragment(boolean flag);
    public void refreshData(String s);
}
