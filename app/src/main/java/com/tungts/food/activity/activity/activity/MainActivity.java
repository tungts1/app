package com.tungts.food.activity.activity.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.tungts.food.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView imgTw,imgFb,imgLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgFb = (ImageView) findViewById(R.id.imgFb);imgFb.setOnClickListener(this);
        imgTw = (ImageView) findViewById(R.id.imgTw);imgTw.setOnClickListener(this);
        imgLogin = (ImageView) findViewById(R.id.imgLogin);imgLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.imgFb:
                changeActivity();
                break;
            case R.id.imgTw:
                changeActivity();
                break;
            case R.id.imgLogin:
                changeActivity();
                break;
        }
    }

    private void changeActivity() {
        Intent intent = new Intent(MainActivity.this,HomeActivity.class);
        startActivity(intent);
    }
}
