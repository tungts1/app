package com.tungts.food.activity.activity.itemstatic;

/**
 * Created by tungts on 7/13/2017.
 */

public class MenuNavi {
    public static final int MENU_HOME = 0;
    public static final int MENU_COMPOSE = 1;
    public static final int MENU_RECIPES = 2;
    public static final int MENU_CATEGORIES = 3;
    public static final int MENU_PROFILE = 4;
    public static final int MENU_SETTINGS = 5;
}
