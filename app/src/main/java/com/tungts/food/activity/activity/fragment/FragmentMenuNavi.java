package com.tungts.food.activity.activity.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tungts.food.R;
import com.tungts.food.activity.activity.interfaces.ItemClickMenuNavi;

import org.w3c.dom.Element;

import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_CATEGORIES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_COMPOSE;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_HOME;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_PROFILE;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_RECIPES;
import static com.tungts.food.activity.activity.itemstatic.MenuNavi.MENU_SETTINGS;

/**
 * Created by tungts on 7/11/2017.
 */

public class FragmentMenuNavi extends Fragment implements View.OnClickListener{

    View root;
    RelativeLayout naviHome,naviCompose,naviRecipes,naviCategories,naviProfile,naviSettings;
    private ItemClickMenuNavi itemClickMenuNavi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_menu_navi,container,false);
        addControls();
        addEvents();
        return root;
    }

    private void addControls() {
        naviHome = (RelativeLayout) root.findViewById(R.id.naviHome);
        naviCompose = (RelativeLayout) root.findViewById(R.id.naviCompose);
        naviRecipes = (RelativeLayout) root.findViewById(R.id.naviRecipes);
        naviCategories = (RelativeLayout) root.findViewById(R.id.naviCategories);
        naviProfile = (RelativeLayout) root.findViewById(R.id.naviProfile);
        naviSettings = (RelativeLayout) root.findViewById(R.id.naviSettings);
    }

    private void addEvents(){
        naviHome.setOnClickListener(this);
        naviCompose.setOnClickListener(this);
        naviRecipes.setOnClickListener(this);
        naviCategories.setOnClickListener(this);
        naviProfile.setOnClickListener(this);
        naviSettings.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.naviHome:
                itemClickMenuNavi.onItemClickMenuNavi(MENU_HOME);
                break;
            case R.id.naviCompose:
                itemClickMenuNavi.onItemClickMenuNavi(MENU_COMPOSE);
                break;
            case R.id.naviRecipes:
                ImageView imgNavi4 = (ImageView) root.findViewById(R.id.imgNavi4);
                imgNavi4.setImageDrawable(getResources().getDrawable(R.drawable.ic_nav4));
                TextView tv4 = (TextView) root.findViewById(R.id.tvNaviCategories);
                tv4.setTextColor(Color.BLACK);
                naviCategories.setBackgroundColor(0xFFFFFF);
                TextView tv3 = (TextView) root.findViewById(R.id.tvNaviRecipes);
                tv3.setTextColor(Color.WHITE);
                ImageView imgNavi3 = (ImageView) root.findViewById(R.id.imgNavi3);
                imgNavi3.setImageDrawable(getResources().getDrawable(R.drawable.ic_nav3_sel));
                naviRecipes.setBackgroundDrawable(getResources().getDrawable(R.drawable.reg_bg));
                itemClickMenuNavi.onItemClickMenuNavi(MENU_RECIPES);
                break;
            case R.id.naviCategories:
                ImageView imgNaviRecipes = (ImageView) root.findViewById(R.id.imgNavi3);imgNaviRecipes.setImageDrawable(getResources().getDrawable(R.drawable.ic_nav3));
                TextView tvRecipes = (TextView) root.findViewById(R.id.tvNaviRecipes);tvRecipes.setTextColor(Color.BLACK);
                naviRecipes.setBackgroundColor(0xFFFFFF);
                ImageView imgNaviCategories = (ImageView) root.findViewById(R.id.imgNavi4);
                imgNaviCategories.setImageDrawable(getResources().getDrawable(R.drawable.ic_nav4_sel));
                TextView tvCategories= (TextView) root.findViewById(R.id.tvNaviCategories);tvCategories.setTextColor(Color.WHITE);
                naviCategories.setBackgroundDrawable(getResources().getDrawable(R.drawable.reg_bg));
                itemClickMenuNavi.onItemClickMenuNavi(MENU_CATEGORIES);
                break;
            case R.id.naviProfile:
                itemClickMenuNavi.onItemClickMenuNavi(MENU_PROFILE);
                break;
            case R.id.naviSettings:
                itemClickMenuNavi.onItemClickMenuNavi(MENU_SETTINGS);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            itemClickMenuNavi = (ItemClickMenuNavi) context;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        itemClickMenuNavi = null;
    }

}
