package com.tungts.food.activity.activity.model;

/**
 * Created by tungts on 7/13/2017.
 */

public class Categories {

    private String nameCat;
    private int numberCat;

    public Categories() {
    }

    public Categories(String nameCat, int numberCat) {
        this.nameCat = nameCat;
        this.numberCat = numberCat;
    }

    public String getNameCat() {
        return nameCat;
    }

    public void setNameCat(String nameCat) {
        this.nameCat = nameCat;
    }

    public int getNumberCat() {
        return numberCat;
    }

    public void setNumberCat(int numberCat) {
        this.numberCat = numberCat;
    }
}
