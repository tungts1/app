package com.tungts.food.activity.activity.model;

import java.io.Serializable;

/**
 * Created by tungts on 7/12/2017.
 */

public class Recipes implements Serializable {

    private String nameFood;
    private String nameUser;

    public Recipes() {
    }

    public Recipes(String nameFood, String nameUser) {
        this.nameFood = nameFood;
        this.nameUser = nameUser;
    }

    public String getNameFood() {
        return nameFood;
    }

    public void setNameFood(String nameFood) {
        this.nameFood = nameFood;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }
}
