package com.tungts.food.activity.activity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tungts.food.R;
import com.tungts.food.activity.activity.interfaces.NaviListener;

/**
 * Created by tungts on 7/11/2017.
 */

public class CustomerActionBar extends RelativeLayout implements View.OnClickListener{

    private NaviListener naviListener;
    private TextView tvTitle;

    public CustomerActionBar(Context context) {
        super(context);
        innitView();
    }

    public CustomerActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        innitView();
    }

    public CustomerActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        innitView();
    }

    private ImageView imgMenuNavi;
    private RelativeLayout drawerMenu;
    private ImageView imgSearch;

    private void innitView(){
        View v = View.inflate(getContext(), R.layout.customer_action_bar, null);
        if (v == null){
            return;
        }
        tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        imgMenuNavi = (ImageView) v.findViewById(R.id.imgMenuNavi);
        imgSearch = (ImageView) v.findViewById(R.id.imgSearch);imgSearch.setOnClickListener(this);
        drawerMenu = (RelativeLayout) v.findViewById(R.id.drawerMenu);drawerMenu.setOnClickListener(this);
        addView(v , new LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT));
    }

    public void setNaviListener(NaviListener naviListener){
        this.naviListener = naviListener;
    }

    public void setTitle(String s){
        tvTitle.setText(s);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.drawerMenu) {
            if (naviListener != null){
                naviListener.onMenuNavi();
            }
        } else if (id == R.id.imgSearch){
            if (naviListener != null){
                naviListener.OnSearch();
            }
        }

    }
}
