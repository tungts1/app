package com.tungts.food.activity.activity.interfaces;

/**
 * Created by tungts on 7/13/2017.
 */

public interface ItemClickMenuNavi{
    void onItemClickMenuNavi(int position);
}