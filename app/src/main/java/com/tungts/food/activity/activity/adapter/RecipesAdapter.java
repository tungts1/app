package com.tungts.food.activity.activity.adapter;
import com.squareup.picasso.Picasso;
import com.tungts.food.R;
import com.tungts.food.activity.activity.interfaces.RecycleViewItemClick;
import com.tungts.food.activity.activity.model.Categories;
import com.tungts.food.activity.activity.model.Recipes;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by tungts on 7/12/2017.
 */

public class RecipesAdapter  extends RecyclerView.Adapter<RecipesAdapter.MyViewHolder>{

    private Context context;
    private List listRecipes;
    private LayoutInflater layoutInflater;
    public static final int RECIPES = 999;
    public static final int CATEGORIES = 1000;
    int type;
    RecycleViewItemClick recycleViewItemClick;

    public RecipesAdapter(Context context, List list,int type){
        this.context = context;
        this.listRecipes = list;
        this.type = type;
        layoutInflater = LayoutInflater.from(context);
    }

    public void updateData(ArrayList arrRecipes){
        listRecipes.clear();
        listRecipes.addAll(arrRecipes);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (type == RECIPES){
            itemView = layoutInflater.inflate(R.layout.item_recipies,null);
            return new RecipesViewHolder(itemView);
        } else if (type == CATEGORIES){
            itemView = layoutInflater.inflate(R.layout.item_categories,null);
            return new CategoriesViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        Log.e("ST",itemType+"");
        if (type == RECIPES) {
            ((RecipesViewHolder) holder).blindData(position);
        } else if (type == CATEGORIES){
            ((CategoriesViewHolder) holder).blindData(position);
        }
    }


    @Override
    public int getItemCount() {
        return listRecipes.size();
    }

    public void setRecycleViewItemClick(RecycleViewItemClick recycleViewItemClick) {
        this.recycleViewItemClick = recycleViewItemClick;
    }

    class RecipesViewHolder extends MyViewHolder{

        private ImageView imgRec,imgUserRec;
        private TextView tvNameFood,tvnameUser;

        public RecipesViewHolder(View itemView) {
            super(itemView);
            imgRec = (ImageView) itemView.findViewById(R.id.imgRec);
            imgUserRec = (ImageView) itemView.findViewById(R.id.imgUserRec);
            tvNameFood = (TextView) itemView.findViewById(R.id.tvNameFood);
            tvnameUser = (TextView) itemView.findViewById(R.id.tvNameUser);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemClick(getAdapterPosition(),RECIPES);
                    }
                }
            });
        }

        public void blindData(int position){
            Recipes recipes = (Recipes) listRecipes.get(position);
            tvNameFood.setText(recipes.getNameFood());
            tvnameUser.setText(recipes.getNameUser());
            Picasso.with(context).load(R.drawable.user1).into(imgUserRec);
            int img = position % 6;
            switch (img) {
                case 1:
                    Picasso.with(context).load(R.drawable.img1).into(imgRec);
                    break;
                case 2:
                    Picasso.with(context).load(R.drawable.img2).into(imgRec);
                    break;
                case 3:
                    Picasso.with(context).load(R.drawable.img3).into(imgRec);
                    break;
                case 4:
                    Picasso.with(context).load(R.drawable.img4).into(imgRec);
                    break;
                case 5:
                    Picasso.with(context).load(R.drawable.img5).into(imgRec);
                    break;
                default:
                    Picasso.with(context).load(R.drawable.img6).into(imgRec);
                    break;
            }
        }
    }

    class CategoriesViewHolder extends MyViewHolder{

        private ImageView imgCat;
        private TextView tvNameCat,tvNumberCat;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            imgCat = (ImageView) itemView.findViewById(R.id.imgCat);
            tvNameCat = (TextView) itemView.findViewById(R.id.tvNameCat);
            tvNumberCat = (TextView) itemView.findViewById(R.id.tvNumberCat);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemClick(getAdapterPosition(),CATEGORIES);
                    }
                }
            });
        }

        public void blindData(int position) {
            Categories categories = (Categories) listRecipes.get(position);
            tvNameCat.setText(categories.getNameCat());
            tvNumberCat.setText(categories.getNumberCat()+"");
            int img = position % 6;
            switch (img) {
                case 1:
                    Picasso.with(context).load(R.drawable.cat1).into(imgCat);
                    break;
                case 2:
                    Picasso.with(context).load(R.drawable.cat2).into(imgCat);
                    break;
                case 3:
                    Picasso.with(context).load(R.drawable.cat3).into(imgCat);
                    break;
                case 4:
                    Picasso.with(context).load(R.drawable.cat4).into(imgCat);
                    break;
                case 5:
                    Picasso.with(context).load(R.drawable.cat5).into(imgCat);
                    break;
                default:
                    Picasso.with(context).load(R.drawable.cat6).into(imgCat);
                    break;
            }
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        public void blindData(int poisiton){

        }
    }
}
